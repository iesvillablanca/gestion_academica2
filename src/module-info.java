/**
 * <p>Este módulo contiene clases y métodos que permiten una gestion academica sencilla</p>
 * @author Alvaro Fenandez Lopez
 * @version 1.0
 * @since 1.0
*/
module gestion.academica {
    requires java.base;
    exports gestionAcademica;
}
