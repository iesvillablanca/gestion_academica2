package gestionAcademica;
/**
 * @author Alvaro Fernandez Lopez
 * La clase Nota representa una nota de un alumno.
 * Contiene información sobre la nota (valor comprendido entre 0 y 10), 
 * la evaluación a la que pertenece (1ª, 2ª o 3ª), 
 * la asignatura de la que es esa nota, 
 * el nombre del alumno y 
 * si está aprobado o no (para aprobar necesita que su nota sea >= 5).
 */
public class Nota {
  private double valor;
  private String evaluacion;
  private String asignatura;
  private String nombreAlumno;
  private boolean aprobado;

  /**
   * Constructor de la clase Nota.
   * 
   * @param valor La nota del alumno comprendida entre 0 y 10.
   * @param evaluacion La evaluación a la que pertenece (1ª, 2ª o 3ª).
   * @param asignatura La asignatura de la que es esa nota.
   * @param nombreAlumno El nombre del alumno.
   * @throws IllegalArgumentException si el valor de la nota no está comprendido entre 0 y 10.
   */
  public Nota(double valor, String evaluacion, String asignatura, String nombreAlumno) {
    if (valor >= 0 && valor <= 10) {
      this.valor = valor;
    } else {
      throw new IllegalArgumentException("La nota debe estar comprendida entre 0 y 10");
    }
    this.evaluacion = evaluacion;
    this.asignatura = asignatura;
    this.nombreAlumno = nombreAlumno;
    this.aprobado = (valor >= 5);
  }

  /**
   * Devuelve el valor de la nota.
   * 
   * @return El valor de la nota.
   */
  public double getValor() {
    return valor;
  }

  /**
   * Devuelve la evaluación a la que pertenece la nota.
   * 
   * @return La evaluación a la que pertenece la nota.
   */
  public String getEvaluacion() {
    return evaluacion;
  }

  /**
   * Devuelve la asignatura de la que es esa nota.
   * 
   * @return La asignatura de la que es esa nota.
   */
  public String getAsignatura() {
    return asignatura;
  }

  /**
   * Devuelve el nombre del alumno.
   * 
   * @return El nombre del alumno.
   */
  public String getNombreAlumno() {
    return nombreAlumno;
  }
 }
