package gestionAcademica;
import java.util.Scanner;
/**
 * Descripcion: clase que representa la fecha de nacimiento del alumno con: dia, mes y año
 * @author Sofia Chavez Cossio
 *
 */

public class Fecha {
	//Atributos
	/**
	 * Dia de la fecha de nacimiento
	 */
	private int dia;
	/**
	 * Mes de la fecha de nacimiento
	 */
	private int mes;
	/**
	 * Año de la fecha de nacimiento 
	 */
	private int anio;
	Scanner leer = new Scanner(System.in);
	
	
	// Constructores
	
	/**
	 * Constructor por defecto no tiene parametros de entrada.
	 */
	public Fecha() {
		dia = 0;
		mes = 0;
		anio = 0;
	}

	/**
	 * Constructor con 3 parametros.
	 * 
	 * @param dia Dia de la fehca de nacimiento.
	 * @param mes Mes de la fecha de nacimiento.
	 * @param anio Año de la fecha de nacimiento.
	 */
	public Fecha(int dia, int mes, int anio) {
		setDia(dia);
		setMes(mes);
		setAnio(anio);
	}
	
	/**       
	 * Constructor copia: tiene como parametro de entrada una referencia a otro objeto de la misma clase.
	 */
	public Fecha (Fecha otro) {
		dia = otro.dia;
		mes = otro.mes;
		anio = otro.anio;
	}
	
	/**
	 * Devuelve el dia de una fecha de nacimiento.
	 * @return dia de la fecha de nacimiento
	 */
	public int getDia() {
		return dia;
	}

	/**
	 * Modifica el dia de una fecha de nacimiento.
	 * @param dia 
	 */
	public void setDia(int dia) {
		if(dia < 0 )
			dia = 0;
		this.dia = dia;
	}
	
	/**
	 * Devuelve el mes de una fecha de nacimiento.
	 * @return mes de la fecha de nacimienton
	 */
	public int getMes() {
		return mes;
	}
	
	/**
	 * Modifica el mes de una fecha de nacimiento.
	 * @param mes
	 */
	public void setMes(int mes) {
		if(mes < 0)
			mes = 0;
		this.mes = mes;
	}
	
	/**
	 * Devuelve el año de una fecha de nacimiento.
	 * @return año de la fecha de nacimiento
	 */
	public int getAnio() {
		return anio;
	}

	/**
	 * Modifica el año de una fecha de nacimiento.
	 * @param anio
	 */
	public void setAnio(int anio) {
		if(anio < 0)
			anio = 0;
		this.anio = anio;
	}
		
	/**
	 * Metodo donde se leen los datos de la fecha.
	 */
	public void leerDatos() {
		do {
			System.out.print("Año: ");
			anio = leer.nextInt();
		}while (anio < 1900 || anio > 3000);
		
		do
		{
			System.out.print("Mes: ");
			mes = leer.nextInt();
		}while (mes <= 1 || mes >= 12);
		
		switch (mes) {
			case 1,3,5,7,8,10,12: do{
									System.out.print("Día: ");
									dia = leer.nextInt();
								}while (dia <= 1 || dia >= 31);
								break;
			case 4,6,9,11: do{
							System.out.print("Día: ");
							dia = leer.nextInt();
							}while (dia <= 1 || dia >= 30);
							break;
			default: if(esBisiesto(anio)) {
						do {
							System.out.print("Día: ");
							dia = leer.nextInt();
						}while (dia <= 1 || dia >=29);
					}
					else {
						do {
							System.out.print("Día: ");
							dia = leer.nextInt();
						}while(dia <= 1 || dia >=28);
					}
			}
		}
	
	/**
	 * Metodo privado para saber si el año es bisiesto
	 * @param anio
	 * @return  <ul>
	 * 			<li>true: si es bisiesto.</li>
	 * 			<li>false: no es bisiesto.</li>
	 * 			</ul>
	 */
	private boolean esBisiesto(int anio) {
		boolean loEs = false;
		if ((anio % 4 == 0) && ((anio % 100 != 0) || (anio % 400 == 0)))
			loEs = true;
		return loEs;
			
	}
		
	/**
	 * Metodo donde se muestran los datos del alumno.
	 */
	public void mostrarDatos() {
		System.out.println(this);
	}

	/**
	 * Metodo para devolver los datos del objeto en un String.
	 */
	public String toString() {
		return  "[" + dia + "/" + mes + "/" + anio + "]";
	}	
}

