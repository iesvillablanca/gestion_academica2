package gestionAcademica;
import java.util.Scanner;
/**
 * Descripcion: la clase representa sobre los siguientes datos del alumno: nombre, idAlumno, dni y fecha de nacimiento
 * @author Sofia Chavez Cossio
 *
 */

public class Alumno {
	//Atributos
	/**
	 * Nombre del alumno
	 */
	private String nombre;
	/**
	 * Id del alumno
	 */
	private String idAlumno;
	/**
	 * Dni del alumno
	 */
	private String dni;
	/**
	 * Fecha de nacimiento del alumno
	 */
	private Fecha fechaNacimiento;
	Scanner leer = new Scanner(System.in);
	
	
	//Constructores
	/**
	 * Constructor por defecto no tiene parametros de entrada.
	 */
	public Alumno () {
		nombre = "";
		idAlumno = "";
		dni = "";
		fechaNacimiento = new Fecha();
	}
	
	/**
     * Constructor con 4 parametros.
     * 
     * @param nombre Nombre del alumno.
     * @param idAlumno Código del alumno.
     * @param dni Documento de identidad del alumno.
     * @param fechaNacimiento fecha en la que nacio el alumno.
     */
	public Alumno (String nombre, String idAlumno, String dni, Fecha fechaNacimiento) {
		this.nombre = nombre;
		this.idAlumno = idAlumno;
		this.dni = dni;
		this.fechaNacimiento = fechaNacimiento;
	}
	
	/**       
	 * Constructor copia: tiene como parametro de entrada una referencia a otro objeto de la misma clase
	 */
	public Alumno(Alumno otro) {
		nombre = otro.nombre;
		idAlumno = otro.idAlumno;
		dni = otro.dni;
		fechaNacimiento = otro.fechaNacimiento;
	}
	
	/**
	 * Devuelve el nombre de un alumno.
	 * @return nombre del alumno
	 */
	public String getNombre() {
		return nombre;
	}
	
	/**
	 * Modifica el nombre de un alumno.
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * Devuelve el id de un alumno.
	 * @return id del alumno
	 */
	public String getIdAlumno() {
		return idAlumno;
	}
	
	/**
	 * Modifica el id de un alumno.
	 * @param idAlumno
	 */
	public void setIdAlumno(String idAlumno) {
		this.idAlumno = idAlumno;
	}
	
	/**
	 * Devuelve el dni de un alumno
	 * @return dni del alumno
	 */
	public String getDni() {
		return dni;
	}
	
	/**
	 * Modifica el dni de un alumno.
	 * @param dni
	 */
	public void setDni(String dni) {
		this.dni = dni;
	}
	
	/**
	 * Devuelve la fecha de nacimiento de un alumno.
	 * @return fecha de nacimiento del alumno
	 */
	public Fecha getFechaNacimiento() {
		return fechaNacimiento;
	}
	
	/**
	 * Modifica la fecha de nacimiento de un alumno.
	 * @param fechaNacimiento
	 */
	public void setFechaNacimiento(Fecha fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	
	/**
	 * Metodo donde se lee la clave del alumno, que en este caso es el id. 
	 * La clave es para identificar a cada alumno.
	 */
	public void leerClave() {
		System.out.print("Id del Alumno: ");
		idAlumno = leer.nextLine();
	}
	
	/**
	 * Metodo donde se leen el resto de datos que tiene el alumno.
	 */
	public void leerOtrosDatos() {
		System.out.print("Nombre: ");
		nombre = leer.nextLine();
		System.out.print("Dni: ");
		dni = leer.nextLine(); 
		fechaNacimiento.leerDatos();
	}
	
	/**
	 * Metodo donde se muestran los datos del alumno.
	 */
	public void mostrarDatos() {
		System.out.println(this);
	}
	
	/**
	 * Metodo para devolver los datos del objeto en un String.
	 */
	public String toString() {
		return "Alumno [nombre= " + nombre + ", idAlumno= " + idAlumno + ", dni= " + dni  + "fecha de nacimiento= " + fechaNacimiento + "]";
	}
	
	/**
	 * Metodo que comprueba si el objeto es distinto de null y si hay dos alumnos con el mismo id.
	 * @param otro
	 * @return <ul>
	 * 			<li>true: si son iguales los id.</li>
	 * 			<li>false: si no son iguales los id.</li>
	 * 			</ul>
	 */
	public boolean equals(Alumno otro) {
		return (otro != null && idAlumno.equalsIgnoreCase(otro.idAlumno));
	}
}
